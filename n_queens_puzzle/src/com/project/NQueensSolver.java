package com.project;

public class NQueensSolver {

    public static void solve(int N){
        int[][] sol = new int[N][N];

        if(solveBoard(sol, 0)){
            printSolution(sol, N);
        } else{
            System.out.println("Could not solve for: " + N);
        }
    }

    private static boolean solveBoard(int[][] board, int col){
        if(isFinished(board, col))
            return true;
        for(int i = 0; i < board.length; i++){
            if(tryRow(board, col, i))
                return true;
        }
        return false;
    }

    private static void printSolution(int[][] sol, int N){
        System.out.println("Solution for " + N + ":");
        for(int i = 0; i < N; i++){
            for(int j = 0; j< N; j++)
                System.out.print(" " + sol[i][j] + " ");
            System.out.println();
        }
    }

    private static boolean tryRow(int[][] board, int col, int i){
        if(!isSafe(board, col, i))
            return false;
        board[i][col] = 1;
        if(!solveBoard(board, col + 1)){
            board[i][col] = 0;
            return false;
        }
        return true;
    }

    private static boolean isFinished(int[][] board, int col){
        return (col >= board.length);
    }

    private static boolean isSafe(int[][] board, int col, int row){
        return rowIsSafe(board, col, row) && topLeftDiagonalIsSafe(board, col, row) && bottomLeftDiagonalIsSafe(board, col, row);
    }

    private static boolean rowIsSafe(int[][] board, int col, int row){
        for(int i = 0; i< col; i++)
            if(board[row][i] == 1)
                return false;
        return true;
    }

    private static boolean topLeftDiagonalIsSafe(int[][] board, int col, int row){
        for(int i = row, j = col; i >= 0 && j >= 0; i--, j--)
            if(board[i][j] == 1)
                return false;
        return true;
    }

    private static boolean bottomLeftDiagonalIsSafe(int[][] board, int col, int row){
        for(int i = row, j = col; j >= 0 && i < board.length; j--, i++)
            if(board[i][j] == 1)
                return false;
        return true;
    }

}
