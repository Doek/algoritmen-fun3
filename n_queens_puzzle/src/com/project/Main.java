package com.project;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        while(true){
            System.out.println("Give size of board: ");
            NQueensSolver.solve(Integer.parseInt(in.nextLine()));
        }
    }
}
