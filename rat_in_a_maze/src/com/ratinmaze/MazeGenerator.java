package com.ratinmaze;

public class MazeGenerator {
    private int[][] standardMaze = {
            { 1, 1, 0, 1, 0 },
            { 0, 1, 0, 1, 0 },
            { 1, 1, 0, 0, 0 },
            { 1, 0, 1, 1, 1 },
            { 1, 1, 1, 0, 1 }
    };

    public Maze getStandardMaze(){
        return new Maze(standardMaze);
    }
}
