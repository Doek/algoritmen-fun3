package com.ratinmaze;

public class Maze {
    private int maze[][];
    private int end;
    private int dimensions;


    public Maze(int [][] maze){
        this.maze = maze;
        this.dimensions = maze.length;
        this.end = dimensions -1;
    }


    public boolean isInMaze(int x, int y)
    {
        return (x >= 0 && x < dimensions && y >= 0 && y < dimensions && maze[x][y] == 1);
    }

    public boolean isAtEnd(int x, int y){
        return x == end && y == end;
    }

    public int[][] getMaze(){
        return maze;
    }

}
