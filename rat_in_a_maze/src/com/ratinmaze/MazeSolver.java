package com.ratinmaze;

public class MazeSolver {

    private void printMatrix(int sol[][], String message)
    {
        System.out.println(message);
        for (int i = 0; i < sol.length; i++) {
            for (int j = 0; j < sol.length; j++)
                System.out.print(" " + sol[i][j] + " ");
            System.out.println();
        }
    }

    public void getSolution(Maze maze){
        int sol[][] = new int[maze.getMaze().length][maze.getMaze().length];

        if(solveMazeUtil(maze, 0, 0, sol))
            printMatrix(sol, "Solution:");
        else{
            printMatrix(maze.getMaze(), "No solution found for: ");
        }
    }

    private boolean solveMazeUtil(Maze maze, int x, int y, int sol[][])
    {
        if (maze.isAtEnd(x, y)) {
            sol[x][y] = 1;
            return true;
        }

        if (maze.isInMaze(x, y)) {
            if(sol[x][y] == 1)
                return false;

            sol[x][y] = 1;
            if (solveMazeUtil(maze, x + 1, y, sol) || solveMazeUtil(maze, x, y + 1, sol) || solveMazeUtil(maze, x - 1, y, sol) || solveMazeUtil(maze, x, y -1, sol)){
                return true;
            }

            sol[x][y] = 0;
            return false;
        }

        return false;
    }


}
