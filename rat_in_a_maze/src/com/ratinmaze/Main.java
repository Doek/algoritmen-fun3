package com.ratinmaze;

public class Main {

    private static MazeGenerator mazeGenerator = new MazeGenerator();
    private static MazeSolver mazeSolver = new MazeSolver();

    public static void main(String[] args) {
        mazeSolver.getSolution(mazeGenerator.getStandardMaze());
    }


}
